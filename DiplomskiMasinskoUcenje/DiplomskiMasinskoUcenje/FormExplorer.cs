﻿using LinearRegression;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace DiplomskiMasinskoUcenje
{
    public partial class FormExplorer : Form
    {
        FileSettings fileTrainingSettingsContainer;
        FileSettings fileTestSettingsContainer;

        ADataReader dataReader;
        ARegression regression;

        FormPredictionResults fpr;
        FormShowData fsdTraining;
        FormShowData fsdTest;


        public FormExplorer()
        {
            InitializeComponent();

            fileTestSettingsContainer = new FileSettings();
            fileTrainingSettingsContainer = new FileSettings();
        }

        private void loadFileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void loadSplitterMethod()
        {
            if (radioUsetrainingTestSet.Checked)
            {
                dataReader = new DataReaderTrainingTestSets(fileTrainingSettingsContainer, fileTestSettingsContainer);
            }
            else if (radioPercentageSplit.Checked)
            {
                dataReader = new DataReaderPercentageSplitter(fileTrainingSettingsContainer, (int)numericPercentage.Value);
            }
            else
            {
                dataReader = new DataReaderTrainingTestSets(fileTrainingSettingsContainer, fileTestSettingsContainer);
            }  
        }

        private void CreateRegressionInstance()
        {
            if(radioUsetrainingTestSet.Checked)
            {
                //linreg = new LinearRegression.LinearRegression(dataSplitter.GetTrainData(), dataSplitter.GetTestData(), comboY.SelectedIndex/*dataSplitter.GetTrainData()[0].Length - 1*/);
                regression = new MultipleLinearRegression(dataReader.TraingingX, dataReader.TrainingY, dataReader.TestX, dataReader.TestY);
                FillComboBox();
                LoadDataInGraph();
                PrintInConsole(regression.TraningDataDescription.ToString());
                PrintInConsole(regression.TestDataDescription.ToString());
                btnStart.Enabled = true;
            }
            else if(radioPercentageSplit.Checked)
            {
                //linreg = new LinearRegression.LinearRegression(dataSplitter.GetTrainData(), dataSplitter.GetTestData(), comboY.SelectedIndex);
                regression = new MultipleLinearRegression(dataReader.TraingingX, dataReader.TrainingY, dataReader.TestX, dataReader.TestY);
                FillComboBox();
                LoadDataInGraph();
                PrintInConsole(regression.TraningDataDescription.ToString());
                PrintInConsole(regression.TestDataDescription.ToString());
                btnStart.Enabled = true;
            }
            else if(radioCrossValidation.Checked)
            {
                regression = new CrossLinearRegression(dataReader.TraingingX, dataReader.TrainingY, dataReader.TestX, dataReader.TestY, (int) numericCrossValidationFolds.Value);
                FillComboBox();
                LoadDataInGraph();
                PrintInConsole(regression.TraningDataDescription.ToString());
                PrintInConsole(regression.TestDataDescription.ToString());
                btnStart.Enabled = true;
            }
        }

        private void PrintInConsole(string v)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("-----------------------------------------------------------------------------\n");
            builder.Append(DateTime.Now.ToShortTimeString() + "\n");
            builder.Append("-----------------------------------------------------------------------------\n");

            rtxtConsole.Text += "\n" + v;
        }

        private void LoadDataInGraph()
        {
            int Xcolumn = comboX.SelectedIndex;
            int Ycolumn = comboY.SelectedIndex;

            GraphPane myPane = graphControl.GraphPane;
            myPane.CurveList.Clear();
            
            double[] x = column(dataReader.TraingingX, Xcolumn);
            double[] y = MatrixOperations.MatrixOps.MatrixToVector(dataReader.TrainingY);

            PointPairList listOfPoints = new PointPairList(x, y);
            LineItem myPoints;

            myPoints = myPane.AddCurve("Points", listOfPoints, Color.Blue, SymbolType.Circle);
            myPoints.Line.IsVisible = false;
            myPoints.Symbol.Border.IsVisible = false;
            myPoints.Symbol.Fill = new Fill(Color.Blue);

            graphControl.AxisChange();
            graphControl.Invalidate();
        }

        private void FillComboBox()
        {
            comboY.SelectedIndexChanged -= comboY_SelectedIndexChanged;
            comboX.SelectedIndexChanged -= comboX_SelectedIndexChanged;

            comboX.Items.Clear();
            comboY.Items.Clear();

            comboX.Items.AddRange(dataReader.Header.Take(dataReader.Header.Length-1).ToArray());
            comboX.SelectedIndex = 0;

            comboY.Items.AddRange(dataReader.Header);
            comboY.SelectedIndex = comboY.Items.Count - 1;

            comboY.SelectedIndexChanged += comboY_SelectedIndexChanged;
            comboX.SelectedIndexChanged += comboX_SelectedIndexChanged;
        }

        private void flfBtnOk_Test_Click(object sender, EventArgs e)
        {
            if (radioUsetrainingTestSet.Checked || radioCrossValidation.Checked)
            {
                if (File.Exists(fileTestSettingsContainer.FileLink) && File.Exists(fileTrainingSettingsContainer.FileLink))
                {
                    loadSplitterMethod();
                    CreateRegressionInstance();
                }
            }            

        }

        private void flfBtnOk_Tranining_Click(object sender, EventArgs e)
        {

            if (radioUsetrainingTestSet.Checked)
            {
                if (File.Exists(fileTestSettingsContainer.FileLink) && File.Exists(fileTrainingSettingsContainer.FileLink))
                {
                    loadSplitterMethod();
                    CreateRegressionInstance();
                }
            }
            else
            {
                loadSplitterMethod();
                CreateRegressionInstance();
            }
        }
        
        public static T[] column<T>(T[][] jaggedArray, int wanted_column)
        {
            T[] columnArray = new T[jaggedArray.Length];
            T[] rowArray;
            for (int i = 0; i < jaggedArray.Length; i++)
            {
                rowArray = jaggedArray[i];
                if (wanted_column < rowArray.Length || wanted_column >= 0)
                    columnArray[i] = rowArray[wanted_column];
            }
            return columnArray;
        }
        //training
        private void trainingSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fileTrainingSettingsContainer = new FileSettings();
            FormLoadFile flf = new FormLoadFile(fileTrainingSettingsContainer, "Load Training Set");
            flf.OkButtonClicked += flfBtnOk_Tranining_Click;

            flf.Show();
        }
        //test
        private void testSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fileTestSettingsContainer = new FileSettings();
            FormLoadFile flf = new FormLoadFile(fileTestSettingsContainer, "Load Test Set");
            flf.OkButtonClicked += flfBtnOk_Test_Click;

            flf.Show();
        }

        private void radioSuppliedTestSet_CheckedChanged(object sender, EventArgs e)
        {
            EnableTestOptionsControls();
        }

        private void radioCrossValidation_CheckedChanged(object sender, EventArgs e)
        {
            EnableTestOptionsControls();
        }

        private void radioPercentageSplit_CheckedChanged(object sender, EventArgs e)
        {
            EnableTestOptionsControls();
        }

        private void EnableTestOptionsControls()
        {
            if (radioUsetrainingTestSet.Checked)
            {
                DisableTestOptionsControls();
                //btnLoadTrainingSet.Enabled = true;
                //btnLoadTestSet.Enabled = true;
            }
            else if (radioCrossValidation.Checked)
            {
                DisableTestOptionsControls();
                numericCrossValidationFolds.Enabled = true;

                //btnLoadTrainingSet.Enabled = true;
                //btnLoadTestSet.Enabled = true;
            }
            else if (radioPercentageSplit.Checked)
            {
                DisableTestOptionsControls();
                numericPercentage.Enabled = true;

                //btnLoadTrainingSet.Enabled = true;
                //btnLoadTestSet.Enabled = true;
            }
        }

        private void DisableTestOptionsControls()
        {
           // btnLoadTestSet.Enabled = false;

            numericCrossValidationFolds.Enabled = false;
            numericPercentage.Enabled = false;
        }

        private void btnLoadTrainingSet_Click(object sender, EventArgs e)
        {
            fileTrainingSettingsContainer = new FileSettings();
            FormLoadFile flf = new FormLoadFile(fileTrainingSettingsContainer, "Load Training Set");
            flf.OkButtonClicked += flfBtnOk_Tranining_Click;

            flf.Show();
        }

        private void btnLoadTestSet_Click(object sender, EventArgs e)
        {
            fileTestSettingsContainer = new FileSettings();
            FormLoadFile flf = new FormLoadFile(fileTestSettingsContainer, "Load Test Set");
            flf.OkButtonClicked += flfBtnOk_Test_Click;

            flf.Show();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (radioUsetrainingTestSet.Checked)
                regression = new MultipleLinearRegression(dataReader.TraingingX, dataReader.TrainingY, dataReader.TestX, dataReader.TestY);
            else if (radioPercentageSplit.Checked)
                regression = new MultipleLinearRegression(dataReader.TraingingX, dataReader.TrainingY, dataReader.TestX, dataReader.TestY);
            else if (radioCrossValidation.Checked)
                regression = new CrossLinearRegression(dataReader.TraingingX, dataReader.TrainingY, dataReader.TestX, dataReader.TestY, (int)numericCrossValidationFolds.Value);

            regression.Learn();

            PrintInConsole(regression.ToString());

            double[] predictedTrainingY = regression.PredictTrainingY();
            double[] perdictedTestY = regression.PredictTestY();

            GraphPane myPane = graphControl.GraphPane;
            myPane.CurveList.Clear();

            double[] x = column(dataReader.TraingingX, comboX.SelectedIndex);
            double[] y = column(dataReader.TrainingY, /*comboY.SelectedIndex*/0);

            PointPairList listOfPoints = new PointPairList(x, y);

            double[] rangeNumbers;
            double[] K = regression.Transform(comboX.SelectedIndex, out rangeNumbers);

            PointPairList list2 = new PointPairList(rangeNumbers, K);

            LineItem myPoints;
            LineItem myCurve;

            myCurve = myPane.AddCurve("Line", list2, Color.Red, SymbolType.Circle);
            myCurve.Line.IsAntiAlias = true;
            myCurve.Line.IsVisible = true;
            myCurve.Symbol.IsVisible = false;
            
            myPoints = myPane.AddCurve("Points", listOfPoints, Color.Blue, SymbolType.Circle);
            myPoints.Line.IsVisible = false;
            myPoints.Symbol.Border.IsVisible = false;
            myPoints.Symbol.Fill = new Fill(Color.Blue);

            graphControl.AxisChange();
            graphControl.Invalidate();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {

        }

        private void comboX_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboX.SelectedIndex > -1 && comboY.SelectedIndex > -1)
                LoadDataInGraph();
        }

        private void comboY_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ovo ne pipaj ce eksplodira 

            string selectedItemX = comboX.SelectedItem.ToString();
            string selectedItemY = comboY.SelectedItem.ToString();
            int tmp = comboY.SelectedIndex;
            if (tmp == dataReader.Header.Length - 1) return;

            if (comboX.SelectedIndex > -1 && comboY.SelectedIndex > -1)
            {

                dataReader.SetDependentColumn(comboY.SelectedIndex);

                comboY.SelectedIndexChanged -= comboY_SelectedIndexChanged;
                comboX.SelectedIndexChanged -= comboX_SelectedIndexChanged;
                comboX.Items.Clear();
                comboY.Items.Clear();

                

                comboX.Items.AddRange(dataReader.Header.Take(dataReader.Header.Length - 1).ToArray());
                comboY.Items.AddRange(dataReader.Header);

                comboX.SelectedIndex = tmp;
                comboY.SelectedIndex = dataReader.Header.Length - 1;

                comboX.SelectedIndexChanged += comboX_SelectedIndexChanged;
                comboY.SelectedIndexChanged += comboY_SelectedIndexChanged;

                LoadDataInGraph();
            }
                
        }

        private void btnPredict_Click(object sender, EventArgs e)
        {
            if ((dataReader.TraingingX == null || dataReader.TrainingY == null))
            {
                MessageBox.Show("First load training data", "Warining", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if ((dataReader.TestX == null || dataReader.TestY == null)/* && (radioCrossValidation.Checked || radioUsetrainingTestSet.Checked)*/)
            {
                MessageBox.Show("First load test data", "Warining", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            
                

            if(regression != null)
            {
                if(regression.Coefficient == null)
                {
                    MessageBox.Show("Learn!!!", "Warining", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                double[] testY = MatrixOperations.MatrixOps.MatrixToVector(dataReader.TestY);
                double[] predictedTestY = regression.PredictTestY();

                double[] trainingY = MatrixOperations.MatrixOps.MatrixToVector(dataReader.TrainingY);
                double[] predictedTrainingY = regression.PredictTrainingY();

                StringBuilder builder = new StringBuilder();
                builder.Append("TestY vs predictedTestY\n");
                for(int i = 0; i < testY.Length; i++)
                {
                    builder.Append("[" + i + "] " + testY[i].ToString("F2") + " - " + predictedTestY[i].ToString("F2") + "\n");
                }

                builder.Append("TrainingY vs predictedTrainingY\n");
                for(int i = 0; i < trainingY.Length; i++)
                {
                    builder.Append("[" + i + "]" + trainingY[i].ToString("F2") + " - " + predictedTrainingY[i].ToString("F2") + "\n");
                }

                PrintInConsole(builder.ToString());

                if(fpr == null)
                {
                    fpr = new FormPredictionResults(trainingY, predictedTrainingY, testY, predictedTestY);
                    fpr.MdiParent = this.MdiParent;
                    fpr.Show();
                    fpr.Location = new Point(930, 10);
                }
                else
                {
                    fpr.RealTrainingY = trainingY;
                    fpr.PredictedY_onTrain = predictedTrainingY;
                    fpr.RealTestY = testY;
                    fpr.PredictedY_onTest = predictedTestY;
                    fpr.RefreshView();

                }

                
            }
        }

        //show training data form
        private void trainingDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataReader == null || dataReader.TraingingX == null || dataReader.TrainingY == null)
            {
                MessageBox.Show("First load training data", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if(fsdTraining == null)
            {
                fsdTraining = new FormShowData(dataReader.TraingingX, dataReader.TrainingY, dataReader.Header);
                fsdTraining.MdiParent = this.MdiParent;
                fsdTraining.Show();
                fsdTraining.Location = new Point(930, 10);
            }
            else
            {
                fsdTraining.DataX = dataReader.TraingingX;
                fsdTraining.DataY = dataReader.TrainingY;
                fsdTraining.Header = dataReader.Header;
                fsdTraining.RefreshView();
            }
        }

        //show test data form
        private void testDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataReader == null || dataReader.TestX == null || dataReader.TestY == null)
            {
                MessageBox.Show("First load test data", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (fsdTest == null)
            {
                fsdTest = new FormShowData(dataReader.TestX, dataReader.TestY, dataReader.Header);
                fsdTest.MdiParent = this.MdiParent;
                fsdTest.Show();
                fsdTest.Location = new Point(930, 10);
            }
            else
            {
                fsdTest.DataX = dataReader.TestX;
                fsdTest.DataY = dataReader.TestY;
                fsdTest.Header = dataReader.Header;
                fsdTest.RefreshView();
            }
        }
    }
}
