﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearRegression
{
    public class DataReaderPercentageSplitter : ADataReader
    {
        double[][] data;
        int percent;

        FileSettings fileSettings;

        public DataReaderPercentageSplitter(FileSettings fileSettings, int percentage)
        {
            this.fileSettings = fileSettings;
            this.percent = percentage;
            
            PrepareData();

            dependentColumn = data[0].Length - 1;
        }

        private void ReadAndSplitData()
        {
            if(File.Exists(fileSettings.FileLink))
            {
                data = MatrixOperations.MatrixOps.MatrixLoad(fileSettings.FileLink, fileSettings.Header, fileSettings.Sep, out this.header);
                this.ShuffleData(data);

                int dataLen = data.Length;
                int trainLen = dataLen / 100 * percent;
                int testLen = dataLen - trainLen;

                double[][] trainingData = data.Take(trainLen).ToArray();
                double[][] testData = data.Skip(trainLen).Take(testLen).ToArray();

                //load trainingX and trainingY
                dependentColumn = trainingData[0].Length - 1;

                int cols = trainingData[0].Length;
                int rows = trainingData.Length;

                TraingingX = MatrixOperations.MatrixOps.MatrixCreate(rows, cols - 1);
                TrainingY = MatrixOperations.MatrixOps.MatrixCreate(rows, 1);

                int j;
                for (j = 0; j < cols - 1; j++)
                {
                    for (int i = 0; i < rows; i++)
                    {
                        TraingingX[i][j] = trainingData[i][j];
                    }
                }

                for (int i = 0; i < rows; i++)
                {
                    TrainingY[i][0] = trainingData[i][j];
                }

                //load testX and testY
                dependentColumn = trainingData[0].Length - 1;

                cols = testData[0].Length;
                rows = testData.Length;

                TestX = MatrixOperations.MatrixOps.MatrixCreate(rows, cols - 1);
                TestY = MatrixOperations.MatrixOps.MatrixCreate(rows, 1);

                for (j = 0; j < cols - 1; j++)
                {
                    for (int i = 0; i < rows; i++)
                    {
                        TestX[i][j] = testData[i][j];
                    }
                }

                for (int i = 0; i < rows; i++)
                {
                    TestY[i][0] = testData[i][j];
                }
            }
        }

        public void ShuffleData(double[][] data)
        {
            Random rnd = new Random((int)DateTime.Now.Ticks);
            int len = data.Length;

            while (len > 2)
            {
                len--;
                int k = rnd.Next(len + 1);
                double[] tempArr = data[k];
                data[k] = data[len];
                data[len] = tempArr;
            }
        }

        public override void SetDependentColumn(int newDependentColumn)
        {
            if (newDependentColumn < TraingingX[0].Length)
            {
                //swap in header
                string tmpHeader = Header[header.Length - 1];
                Header[header.Length - 1] = Header[newDependentColumn];
                Header[newDependentColumn] = tmpHeader;

                //swap in trainingX and trainingY
                if (TraingingX != null && TrainingY != null)
                {
                    double tmpDouble;
                    int rows = TraingingX[0].Length;
                    for (int i = 0; i < rows; i++)
                    {
                        tmpDouble = TraingingX[i][newDependentColumn];
                        TraingingX[i][newDependentColumn] = TrainingY[i][0];
                        TrainingY[i][0] = tmpDouble;
                    }
                }

                //swap in testX and testY
                if (TestX != null && TestY != null)
                {
                    double tmpDouble;
                    int rows = TestX[0].Length;
                    for (int i = 0; i < rows; i++)
                    {
                        tmpDouble = TestX[i][newDependentColumn];
                        TestX[i][newDependentColumn] = TestY[i][0];
                        TestY[i][0] = tmpDouble;
                    }
                }

             //   this.dependentColumn = newDependentColumn;
            }
        }

        public override void PrepareData()
        {
            ReadAndSplitData();
        }
    }
}
