﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearRegression
{
    public class FileSettings
    {
        string fileLink = null;
        bool header = true;
        string sep = "";

        public string FileLink { get => fileLink; set => fileLink = value; }
        public bool Header { get => header; set => header = value; }
        public string Sep { get => sep; set => sep = value; }
    }
}
