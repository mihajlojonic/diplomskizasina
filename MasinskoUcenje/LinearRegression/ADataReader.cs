﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearRegression
{
    public abstract class ADataReader
    {
        protected string[] header;

        double[][] traingingX;
        double[][] trainingY;

        double[][] testX;
        double[][] testY;

        protected int dependentColumn;

        public string[] Header { get => header; set => header = value; }
        public double[][] TraingingX { get => traingingX; set => traingingX = value; }
        public double[][] TrainingY { get => trainingY; set => trainingY = value; }
        public double[][] TestX { get => testX; set => testX = value; }
        public double[][] TestY { get => testY; set => testY = value; }

        public abstract void SetDependentColumn(int newDependentColumn);

        public abstract void PrepareData();

    }
}
