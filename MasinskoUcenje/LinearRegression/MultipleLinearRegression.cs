﻿using MatrixOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearRegression
{
    public class MultipleLinearRegression : ARegression
    {
        public MultipleLinearRegression(double[][] trainingX, double[][] trainingY, double[][] testX, double[][] testY) : base(trainingX, trainingY, testX, testY)
        {
            this.trainingX = trainingX;
            this.trainingY = trainingY;
            this.testX = testX;
            this.testY = testY;

            this.TraningDataDescription = new DataDescription(trainingX, trainingY);
            this.TestDataDescription = new DataDescription(testX, testY);
        }

        public override double[] Learn()
        {
            GenerateDesignMatrix();

            int rows = designMatrix.Length;
            int cols = designMatrix[0].Length;
            double[][] X = MatrixOps.MatrixCreate(rows, cols - 1);
            double[][] Y = MatrixOps.MatrixCreate(rows, 1);

            int j;
            for (int i = 0; i < rows; i++)
            {
                for (j = 0; j < cols - 1; j++)
                {
                    X[i][j] = designMatrix[i][j];
                }
                Y[i][0] = designMatrix[i][j];
            }

            // 2. B = inv(Xt * X) * Xt * y
            double[][] Xt = MatrixOps.MatrixTranspose(X);
            double[][] XtX = MatrixOps.MatrixProduct(Xt, X);
            double[][] inv = MatrixOps.MatrixInverse(XtX);
            double[][] invXt = MatrixOps.MatrixProduct(inv, Xt);

            double[][] mResult = MatrixOps.MatrixProduct(invXt, Y);
            Coefficient = MatrixOps.MatrixToVector(mResult);

            return Coefficient;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("\nCoefficients: \n");
            for (int i = 0; i < Coefficient.Length; i++)
            {
                builder.Append(Coefficient[i].ToString("F2") + "  ");
            }

            builder.Append("\nR squared: " + RSquaredTraining().ToString("F2"));

            builder.Append("\n");

            return builder.ToString();
        }

    }
}
