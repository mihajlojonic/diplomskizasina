﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearRegression
{
    public class CrossResultByFold
    {
        double[] RSquareds;

        double mean;
        double stdev;
        
        public CrossResultByFold(int numFolds)
        {
            RSquareds = new double[numFolds];
        }

        public void SetValue(int numFold, double value)
        {
            if (numFold < 0 || numFold >= RSquareds.Length || RSquareds == null)
                return;
            this.RSquareds[numFold] = value;
        }

        public override string ToString()
        {
            Calculate();
            StringBuilder builder = new StringBuilder();
            builder.Append("\nCrossResultByFold: \n");
            builder.Append("RSquared errors by fold:\n");
            for(int i = 0; i < RSquareds.Length; i++)
            {
                builder.Append(i.ToString() + " fold: " + RSquareds[i].ToString("F2") + "\n");
            }
            builder.Append("Mean: " + mean.ToString("F2"));
            builder.Append("Stdev: " + stdev.ToString("F2"));

            return builder.ToString();
        }

        private void Calculate()
        {
                double sumValue = 0.0;
                double meanValue = 0.0;
                double stdevValue = 0.0;
                for (int j = 0; j < RSquareds.Length; j++)
                {
                    sumValue += RSquareds[j];
                }

                meanValue = sumValue / RSquareds.Length;

                double sumOfSqrt = 0.0;
                for (int j = 0; j < RSquareds.Length; j++)
                {
                    sumOfSqrt += (RSquareds[j] - meanValue) * (RSquareds[j] - meanValue);
                }

                sumOfSqrt = sumOfSqrt / RSquareds.Length;
                stdevValue = Math.Sqrt(sumOfSqrt);

                mean = meanValue;
                stdev = stdevValue;
        }

    }

    public class CrossLinearRegression : ARegression
    {
        int numFolds;

        CrossResultByFold crossResults;

        public CrossLinearRegression(double[][] trainingX, double[][] trainingY, double[][] testX, double[][] testY, int numFolds) : base(trainingX,trainingY,testX,testY)
        {
            this.trainingX = trainingX;
            this.trainingY = trainingY;
            this.testX = testX;
            this.testY = testY;

            this.numFolds = numFolds;

            this.TraningDataDescription = new DataDescription(trainingX, trainingY);
            this.TestDataDescription = new DataDescription(testX, testY);

            crossResults = new CrossResultByFold(numFolds);
        }

        public override double[] Learn()
        {
            GenerateDesignMatrix();
            double rSqaredMax = double.MinValue;
            double[] coefTmp = new double[trainingX[0].Length]; 
            for(int i = 0; i < numFolds; i++)
            {
                double[][] trainFoldX;
                double[][] trainFoldY;
                double[][] testFoldX;
                double[][] testFoldY;

                GetFold(out trainFoldX, out trainFoldY, out testFoldX, out testFoldY, i);

                double[][] Xt = MatrixOperations.MatrixOps.MatrixTranspose(trainFoldX);
                double[][] XtX = MatrixOperations.MatrixOps.MatrixProduct(Xt, trainFoldX);
                double[][] inv = MatrixOperations.MatrixOps.MatrixInverse(XtX);
                double[][] invXt = MatrixOperations.MatrixOps.MatrixProduct(inv, Xt);

                double[][] mResult = MatrixOperations.MatrixOps.MatrixProduct(invXt, trainFoldY);
                coefTmp = MatrixOperations.MatrixOps.MatrixToVector(mResult);

                double[] predictedY = Predict(testFoldX, coefTmp);

                double rSquared = RSquared(coefTmp, testFoldX, testFoldY);

                if(rSquared > rSqaredMax)
                {
                    this.Coefficient = coefTmp;
                }

                crossResults.SetValue(i, rSquared);
            }

            return Coefficient;
        }

        private void GetFold(out double[][] trainFoldX, out double[][] trainFoldY, out double[][] testFoldX, out double[][] testFoldY, int fold)
        {
            int rows = designMatrix.Length;
            int fromRow = rows / numFolds * fold;
            int toRow = fromRow + rows / numFolds;

            if (toRow >= rows) toRow = rows - 1;

            trainFoldX = MatrixOperations.MatrixOps.MatrixCreate(rows - (toRow - fromRow), designMatrix[0].Length - 1);
            trainFoldY = MatrixOperations.MatrixOps.MatrixCreate(rows - (toRow - fromRow), 1);

            testFoldX = MatrixOperations.MatrixOps.MatrixCreate(toRow - fromRow, designMatrix[0].Length - 1);
            testFoldY = MatrixOperations.MatrixOps.MatrixCreate(toRow - fromRow, 1);

            //set first col on 1.0
            trainFoldX = MatrixOperations.MatrixOps.SetColumnOnValue(trainFoldX, 0, 1.0);
            trainFoldY = MatrixOperations.MatrixOps.SetColumnOnValue(trainFoldY, 0, 1.0);

            testFoldX = MatrixOperations.MatrixOps.SetColumnOnValue(testFoldX, 0, 1.0);
            testFoldY = MatrixOperations.MatrixOps.SetColumnOnValue(testFoldY, 0, 1.0);

            int testRowIndex = 0;
            int trainingRowIndex = 0;
            for(int i = 0; i < rows; i++)
            {
                if(i >= fromRow && i < toRow)
                {
                    for(int j = 0; j < designMatrix[0].Length; j++)
                    {
                        if (j == designMatrix[0].Length - 1)
                        {
                            testFoldY[testRowIndex][0] = designMatrix[i][j];
                        }
                        else
                        {
                            testFoldX[testRowIndex][j] = designMatrix[i][j];
                        }
                    }

                    testRowIndex++;
                }
                else
                {
                    for(int j = 0; j < designMatrix[0].Length; j++)
                    {
                        if(j == designMatrix[0].Length - 1)
                        {
                            trainFoldY[trainingRowIndex][0] = designMatrix[i][j];
                        }
                        else
                        {
                            trainFoldX[trainingRowIndex][j] = designMatrix[i][j];
                        }
                    }

                    trainingRowIndex++;
                }
            }

        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append(crossResults.ToString() + "\n");

            builder.Append("Coefficients: \n");
            for(int i = 0; i < Coefficient.Length; i++)
            {
                builder.Append(Coefficient[i].ToString("F2") + "  ");
            }

            builder.Append("\n");

            return builder.ToString();
        }
    }
}
