﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearRegression
{
    public abstract class ARegression
    {
        protected double[][] trainingX;
        protected double[][] trainingY;

        protected double[][] testX;
        protected double[][] testY;

        protected double[][] designMatrix;
        protected double[] predictedTestY;
        protected double[] predictedTrainingY;
        private double[] coefficient = null;
        //dependant column is always last column

        protected double rSquared;

        public DataDescription traningDataDescription;
        public DataDescription testDataDescription;

        public DataDescription TraningDataDescription { get => traningDataDescription; set => traningDataDescription = value; }
        public DataDescription TestDataDescription { get => testDataDescription; set => testDataDescription = value; }
        public double[] Coefficient { get => coefficient; set => coefficient = value; }

        public ARegression(double[][] trainingX, double[][] trainingY, double[][] testX, double[][] testY)
        {
            this.trainingX = trainingX;
            this.trainingY = trainingY;
            this.testX = testX;
            this.testY = testY;

            this.TraningDataDescription = new DataDescription(trainingX, trainingY);
            this.TestDataDescription = new DataDescription(testX, testY);
        }

        protected void GenerateDesignMatrix()
        {
            int rowsX = trainingX.Length;
            int colsX = trainingX[0].Length;
            this.designMatrix = MatrixOperations.MatrixOps.MatrixCreate(rowsX, colsX + 2);

            for (int i = 0; i < rowsX; i++)
                this.designMatrix[i][0] = 1.0;

            int j;
            for (j = 1; j < colsX + 1; j++)
                for (int i = 0; i < rowsX; i++)
                    this.designMatrix[i][j] = trainingX[i][j - 1];

            for (int i = 0; i < rowsX; i++)
                this.designMatrix[i][j] = trainingY[i][0];
        }

        public double[] Transform(int columnNum, out double[] rangeNumbers)
        {
            List<double> ret = new List<double>();

            double K = Coefficient[0];
            for (int i = 0; i < TraningDataDescription.mean.Length - 1; i++)
                K += TraningDataDescription.mean[i] * Coefficient[i + 1];

            K -= TraningDataDescription.mean[columnNum] * Coefficient[columnNum + 1];


            int min = (int)TraningDataDescription.min[columnNum] - 3;
            int max = (int)TraningDataDescription.max[columnNum] + 3;

            rangeNumbers = new double[max - min];

            int m = 0;
            for (int x = min; x < max; x++)
            {
                ret.Add(x * Coefficient[columnNum + 1] + K);
                rangeNumbers[m++] = x;
            }

            return ret.ToArray();
        }

        public double[] PredictTestY()
        {
            if (testX == null || Coefficient == null) return null;

            this.predictedTestY = new double[testY.Length];

            int rowIndex = 0;
            foreach (double[] testRow in testX)
            {
                double result = Coefficient[0];

                for (int coefIndex = 1; coefIndex < Coefficient.Length; coefIndex++)
                {
                    result += testRow[coefIndex - 1] * Coefficient[coefIndex];
                }

                this.predictedTestY[rowIndex++] = result;
            }

            return predictedTestY;
        }

        public double[] Predict(double[][] dataX, double[] coef)
        {
            double[] predictedY = new double[dataX.Length];

            int rowIndex = 0;
            foreach (double[] testRow in dataX)
            {
                double result = 12345.0; //= coef[0];
                //////////////
                for (int coefIndex = 0; coefIndex < coef.Length /*-1*/; coefIndex++)
                    result += testRow[coefIndex] * coef[coefIndex/* + 1*/];

                predictedY[rowIndex++] = result;
            }

            return predictedY;
        }

        public double[] PredictTrainingY()
        {
            if (trainingX == null || Coefficient == null) return null;

            this.predictedTrainingY = new double[trainingY.Length];

            int rowIndex = 0;
            foreach (double[] testRow in trainingX)
            {
                double result = Coefficient[0];

                for (int coefIndex = 1; coefIndex < Coefficient.Length; coefIndex++)
                {
                    result += testRow[coefIndex - 1] * Coefficient[coefIndex];
                }

                this.predictedTrainingY[rowIndex++] = result;
            }

            return predictedTrainingY;
        }

        public double RSquaredTraining()
        {
            PredictTrainingY();
            // 'coefficient of determination'
            int rows = trainingX.Length;
            int cols = trainingX[0].Length;

            // 1. compute mean of y
            double ySum = 0.0;
            for (int i = 0; i < rows; ++i)
                ySum += trainingY[i][0]; // last column
            double yMean = ySum / rows;

            // 2. sum of squared residuals & tot sum squares
            double ssr = 0.0;
            double sst = 0.0;
            double y; // actual y value
            double predictedY; // using the coef[] 
            for (int i = 0; i < rows; ++i)
            {
                y = this.trainingY[i][0];
                predictedY = this.predictedTrainingY[i];

                ssr += (y - predictedY) * (y - predictedY);
                sst += (y - yMean) * (y - yMean);
            }

            if (sst == 0.0)
                throw new Exception("All y values equal");
            else
            {
                rSquared = 1.0 - (ssr / sst);
                return rSquared;
            }
        }


        public double RSquaredTest()
        {
            PredictTestY();
            
            int rows = testX.Length;
            int cols = testX[0].Length;

         
            double ySum = 0.0;
            for (int i = 0; i < rows; ++i)
                ySum += testY[i][0]; 
            double yMean = ySum / rows;

          
            double ssr = 0.0;
            double sst = 0.0;
            double y; 
            double predictedY; 
            for (int i = 0; i < rows; ++i)
            {
                y = this.testY[i][0];
                predictedY = this.predictedTestY[i];

                ssr += (y - predictedY) * (y - predictedY);
                sst += (y - yMean) * (y - yMean);
            }

            if (sst == 0.0)
                throw new Exception("All y values equal");
            else
            {
                rSquared = 1.0 - (ssr / sst);
                return rSquared;
            }
        }

        public double RSquared(double[] coefTmp, double[][] testX, double[][] testY)
        {
            int rows = testX.Length;
            int cols = testX[0].Length;

            // 1. compute mean of y
            double ySum = 0.0;
            for (int i = 0; i < rows; ++i)
                ySum += testY[i][0]; // last column
            double yMean = ySum / rows;

            // 2. sum of squared residuals & tot sum squares
            double ssr = 0.0;
            double sst = 0.0;
            double y; // actual y value
            //double predictedY; // using the coef[] 
            for (int i = 0; i < rows; ++i)
            {
                y = testY[i][0];

                double predictedY = 0.0;
                for (int j = 0; j < cols; j++)
                    predictedY += coefTmp[j] * testX[i][j];

                ssr += (y - predictedY) * (y - predictedY);
                sst += (y - yMean) * (y - yMean);
            }

            if (sst == 0.0)
                throw new Exception("All y values equal");
            else
            {
                rSquared = 1.0 - (ssr / sst);
                return rSquared;
            }
        }

        public abstract double[] Learn();
        public override abstract string ToString();

    }
}
