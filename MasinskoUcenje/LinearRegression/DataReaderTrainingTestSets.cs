﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearRegression
{
    public class DataReaderTrainingTestSets : ADataReader
    {
        double[][] trainingData;
        double[][] testData;

        FileSettings trainingFileSettings;
        FileSettings testFileSettings;

        public FileSettings TrainingFileSettings { get => trainingFileSettings; set => trainingFileSettings = value; }
        public FileSettings TestFileSettings { get => testFileSettings; set => testFileSettings = value; }

        public DataReaderTrainingTestSets(FileSettings trainingFileSettings, FileSettings testFileSettings)
        {
            this.trainingFileSettings = trainingFileSettings;
            this.testFileSettings = testFileSettings;
            
            PrepareData();

            dependentColumn = trainingData[0].Length - 1;
        }

        private void ReadTrainingFile()
        {
            if(File.Exists(TrainingFileSettings.FileLink))
            {
                this.trainingData = MatrixOperations.MatrixOps.MatrixLoad(TrainingFileSettings.FileLink, TrainingFileSettings.Header, TrainingFileSettings.Sep, out this.header);
                dependentColumn = trainingData[0].Length - 1;

                int cols = trainingData[0].Length;
                int rows = trainingData.Length;

                TraingingX = MatrixOperations.MatrixOps.MatrixCreate(rows, cols - 1);
                TrainingY = MatrixOperations.MatrixOps.MatrixCreate(rows, 1);

                int j;
                for(j = 0; j < cols - 1; j++)
                {
                    for(int i = 0; i < rows; i++)
                    {
                        TraingingX[i][j] = trainingData[i][j];
                    }
                }

                for(int i = 0; i < rows; i++)
                {
                    TrainingY[i][0] = trainingData[i][j];
                }
            }
        }

        private void ReadTestFile()
        {
            if(TestFileSettings != null)
            if(File.Exists(TestFileSettings.FileLink))
            {
                this.testData = MatrixOperations.MatrixOps.MatrixLoad(TestFileSettings.FileLink, TestFileSettings.Header, TestFileSettings.Sep, out this.header);
                dependentColumn = trainingData[0].Length - 1;

                int cols = testData[0].Length;
                int rows = testData.Length;

                TestX = MatrixOperations.MatrixOps.MatrixCreate(rows, cols - 1);
                TestY = MatrixOperations.MatrixOps.MatrixCreate(rows, 1);

                int j;
                for(j = 0; j < cols - 1; j++)
                {
                    for(int i = 0; i < rows; i++)
                    {
                        TestX[i][j] = testData[i][j];
                    }
                }

                for(int i = 0; i < rows; i++)
                {
                    TestY[i][0] = testData[i][j];
                }
            }
        }

        public override void SetDependentColumn(int newDependentColumn)
        {
            if (newDependentColumn < TraingingX[0].Length)
            {
                //swap in header
                string tmpHeader = Header[dependentColumn];
                Header[dependentColumn] = Header[newDependentColumn];
                Header[newDependentColumn] = tmpHeader;

                //swap in trainingX and trainingY
                if (TraingingX != null && TrainingY != null)
                {
                    double tmpDouble;
                    int rows = TraingingX[0].Length;
                    for (int i = 0; i < rows; i++)
                    {
                        tmpDouble = TraingingX[i][newDependentColumn];
                        TraingingX[i][newDependentColumn] = TrainingY[i][0];
                        TrainingY[i][0] = tmpDouble;
                    }
                }

                //swap in testX and testY
                if (TestX != null && TestY != null)
                {
                    double tmpDouble;
                    int rows = TestX[0].Length;
                    for (int i = 0; i < rows; i++)
                    {
                        tmpDouble = TestX[i][newDependentColumn];
                        TestX[i][newDependentColumn] = TestY[i][0];
                        TestY[i][0] = tmpDouble;
                    }
                }

                this.dependentColumn = newDependentColumn;
            }
        }

        public override void PrepareData()
        {
            ReadTrainingFile();
            ReadTestFile();
        }
    }
}
