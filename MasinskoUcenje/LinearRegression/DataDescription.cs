﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearRegression
{
    public class DataDescription
    {
        double[][] data;

        public double[] mean;
        public double[] stdev;
        public double[] min;
        public double[] max;
        public double[] range;
        public double[,] corr;

        public DataDescription(double[][] data)
        {
            if(data != null)
            {
                this.data = data;

                int numOfColumns = data[0].Length;

                mean = new double[numOfColumns];
                stdev = new double[numOfColumns];
                min = new double[numOfColumns];
                max = new double[numOfColumns];
                range = new double[numOfColumns];
                Calculate();
                Correlation();
            }
            
        }

        public DataDescription(double[][] dataX, double[][] dataY)
        {
            if(dataX != null)
            {
                int numOfColumns = dataX[0].Length + 1;
                this.data = MatrixOperations.MatrixOps.MatrixCreate(dataX.Length, numOfColumns);

                for (int j = 0; j < numOfColumns; j++)
                {
                    if (j == numOfColumns - 1)
                    {
                        for (int i = 0; i < dataY.Length; i++)
                        {
                            this.data[i][j] = dataY[i][0];
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dataX.Length; i++)
                        {
                            this.data[i][j] = dataX[i][j];
                        }
                    }
                }

                mean = new double[numOfColumns];
                stdev = new double[numOfColumns];
                min = new double[numOfColumns];
                max = new double[numOfColumns];
                range = new double[numOfColumns];
                Calculate();
                Correlation();
            }
            
        }

        private void Calculate()
        {
            for(int i = 0; i < data[0].Length; i++)
            {
                double minValue = double.MaxValue;
                double maxValue = double.MinValue;
                double sumValue = 0.0;
                double meanValue = 0.0;
                double stdevValue = 0.0;
                for (int j = 0; j < data.Length; j++)
                {
                    if (data[j][i] < minValue) minValue = data[j][i];
                    if (data[j][i] > maxValue) maxValue = data[j][i];

                    sumValue += data[j][i];
                }

                meanValue = sumValue / data.Length;

                double sumOfSqrt = 0.0;
                for(int j = 0; j < data.Length; j++)
                {
                    sumOfSqrt += (data[j][i] - meanValue) * (data[j][i] - meanValue);
                }

                sumOfSqrt = sumOfSqrt / data.Length;
                stdevValue = Math.Sqrt(sumOfSqrt);

                mean[i] = meanValue;
                stdev[i] = stdevValue;
                min[i] = minValue;
                max[i] = maxValue;
                range[i] = maxValue - minValue;
            }
        }

        private void Correlation()
        {
            int cols = data[0].Length;
            corr = new double[cols, cols];
            for(int i = 0; i < cols; i++)
            {
                for(int j = 0; j < cols; j++)
                {
                    corr[i, j] = CorrelationPair(i, j);
                }
            }

            /*
            int rows = data.Length;
            int cols = data[0].Length;

            double N = rows;
            corr = new double[cols, cols];
            for (int i = 0; i < cols; i++)
            {
                for (int j = i; j < cols; j++)
                {
                    double c = 0.0;
                    for (int k = 0; k < rows; k++)
                    {
                        double a = z(data[k][j], mean[j], stdev[j]);
                        double b = z(data[k][i], mean[i], stdev[i]);
                        c += a * b;
                    }
                    c /= N - 1.0;
                    corr[i, j] = c;
                    corr[j, i] = c;
                }
            }*/
        }

        private double CorrelationPair(int x, int y)
        {
            int rows = data.Length;
            int cols = data[0].Length;

            double sumX = 0;
            double sumY = 0;
            double sumXY = 0;
            double sumXSquared = 0;
            double sumYSquared = 0;

            for(int i = 0; i < rows; i++)
            {
                sumX += data[i][x];
                sumY += data[i][y];
                sumXY += data[i][x] * data[i][y];
                sumXSquared += data[i][x] * data[i][x];
                sumYSquared += data[i][y] * data[i][y];
            }
            double gore = rows * sumXY - sumX * sumY;
            double doleLevo = rows * sumXSquared - sumX * sumX;
            double doleDesno = rows*sumYSquared - sumY * sumY;

            return gore / (Math.Sqrt(doleLevo*doleDesno));
        }

        private double z(double v, double mean, double sdev)
        {
            if (sdev == 0)
                sdev = 1e-12;

            return (v - mean) / sdev;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("======================================");
            if(mean != null)
            {
                builder.Append("\nMean:\n");
                for (int i = 0; i < mean.Length; i++)
                    builder.Append(mean[i].ToString("F2").PadRight(7));
                builder.Append("\n");
            }
            
            if(stdev != null)
            {
                builder.Append("\nStdev:\n");
                for (int i = 0; i < stdev.Length; i++)
                    builder.Append(stdev[i].ToString("F2").PadRight(7));
                builder.Append("\n");
            }
            
            if(min != null)
            {
                builder.Append("\nMin:\n");
                for (int i = 0; i < min.Length; i++)
                    builder.Append(min[i].ToString("F2").PadRight(7));
                builder.Append("\n");
            }
                

            if(max != null)
            {
                builder.Append("\nMax:\n");
                for (int i = 0; i < max.Length; i++)
                    builder.Append(max[i].ToString("F2").PadRight(7));
                builder.Append("\n");
            }
            
            if(range != null)
            {
                builder.Append("\nRange:\n");
                for (int i = 0; i < range.Length; i++)
                    builder.Append(range[i].ToString("F2").PadRight(7));
                builder.Append("\n");
            }
            
            if(corr != null)
            {
                builder.Append("======================================");
                builder.Append("\nCorrelation:");
                for (int i = 0; i < corr.GetLength(0); i++)
                {

                    builder.Append("\n");
                    for (int j = 0; j < corr.GetLength(1); j++)
                    {
                        builder.Append(corr[i, j].ToString("F2").PadRight(10));
                    }
                }
            }
            
            return builder.ToString();
        }

    }
}
