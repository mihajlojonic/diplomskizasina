﻿using LinearRegression;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace DiplomskiMasinskoUcenje
{
    public partial class FormPredictionResults : Form
    {
        double[] realTrainingY;
        double[] predictedY_onTrain;
        double[] realTestY;
        double[] predictedY_onTest;

        string dependentColumn = "";

        public double[] RealTrainingY { get => realTrainingY; set => realTrainingY = value; }
        public double[] PredictedY_onTrain { get => predictedY_onTrain; set => predictedY_onTrain = value; }
        public double[] RealTestY { get => realTestY; set => realTestY = value; }
        public double[] PredictedY_onTest { get => predictedY_onTest; set => predictedY_onTest = value; }

        public FormPredictionResults(double[] realTrainingY, double[] predictedY_onTrain, double[] realTestY, double[] predictedY_onTest, string dependentColumn)
        {
            InitializeComponent();

            this.RealTrainingY = realTrainingY;
            this.PredictedY_onTrain = predictedY_onTrain;
            this.RealTestY = realTestY;
            this.PredictedY_onTest = predictedY_onTest;

            this.dependentColumn = dependentColumn;

            LoadTrainingPointsGraph();
            LoadTestPointsInGraph();

            LoadDataInGrid();
        }

        private void LoadDataInGrid()
        {
            gridTrainingY.Columns.Clear();

            gridTrainingY.Columns.Add("realTrainingY", "TrainingY");
            gridTrainingY.Columns.Add("predictedTrainingY", "PredictedY");
            gridTrainingY.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            gridTrainingY.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            gridTrainingY.RowHeadersVisible = false;

            gridTestY.Columns.Clear();

            gridTestY.Columns.Add("realTestY", "TestY");
            gridTestY.Columns.Add("predictedTestY", "PredictedY");
            gridTestY.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            gridTestY.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            gridTestY.RowHeadersVisible = false;


            for (int i = 0; i < RealTrainingY.Length; i++)
                gridTrainingY.Rows.Add(RealTrainingY[i], PredictedY_onTrain[i]);

            for(int i = 0; i < RealTestY.Length; i++)
                gridTestY.Rows.Add(RealTestY[i], PredictedY_onTest[i]);
        }

        public void RefreshView()
        {
            LoadTrainingPointsGraph();
            LoadTestPointsInGraph();

            LoadDataInGrid();
        }

        private void LoadTrainingPointsGraph()
        {
            GraphPane trainPane = graphControlTrainingSet.GraphPane;

            if (trainPane == null)
                return;

            trainPane.CurveList.Clear();

            trainPane.Title.Text = "RealTrainingY vs PredictedTrainingY (" + dependentColumn + ")";
            trainPane.XAxis.Title.Text = "RealTrainingY (" + dependentColumn + ")";
            trainPane.YAxis.Title.Text = "PredictedTrainingY (" + dependentColumn + ")";

            PointPairList train_listOfPoints = new PointPairList(RealTrainingY, PredictedY_onTrain);

            LineItem pointsTrain;

            pointsTrain = trainPane.AddCurve("Points", train_listOfPoints, Color.Blue, SymbolType.Circle);
            pointsTrain.Line.IsVisible = false;
            pointsTrain.Symbol.Border.IsVisible = false;
            pointsTrain.Symbol.Fill = new Fill(Color.Blue);

            graphControlTrainingSet.AxisChange();
            graphControlTrainingSet.Invalidate();

            LoadLineTrainingGraph();
        }

        private void LoadTestPointsInGraph()
        {
            GraphPane testPane = graphControlTestSet.GraphPane;

            if (testPane == null)
                return;

            testPane.CurveList.Clear();

            testPane.Title.Text = "RealTestY vs PredictedTestY (" + dependentColumn + ")";
            testPane.XAxis.Title.Text = "RealTestY (" + dependentColumn + ")";
            testPane.YAxis.Title.Text = "PredictedTestY (" + dependentColumn + ")";

            PointPairList test_listOfPoints = new PointPairList(RealTestY, PredictedY_onTest);

            LineItem pointsTest;

            pointsTest = testPane.AddCurve("Points", test_listOfPoints, Color.Blue, SymbolType.Circle);
            pointsTest.Line.IsVisible = false;
            pointsTest.Symbol.Border.IsVisible = false;
            pointsTest.Symbol.Fill = new Fill(Color.Blue);

            graphControlTestSet.AxisChange();
            graphControlTestSet.Invalidate();

            LoadLineTestGraph();
        }

        private void LoadLineTrainingGraph()
        {
            MultipleLinearRegression regresion = new MultipleLinearRegression(
                MatrixOperations.MatrixOps.ArrrayToSingleColumnMatrix(RealTrainingY),
                MatrixOperations.MatrixOps.ArrrayToSingleColumnMatrix(PredictedY_onTrain),
                null,null);

            regresion.Learn();

            double[] rangeNumbers;
            double[] K = regresion.Transform(0, out rangeNumbers);

            GraphPane pane = graphControlTrainingSet.GraphPane;
            PointPairList linePoints = new PointPairList(rangeNumbers, K);

            LineItem myLine;

            myLine = pane.AddCurve("Line", linePoints, Color.Red, SymbolType.Circle);
            myLine.Line.IsAntiAlias = true;
            myLine.Line.IsVisible = true;
            myLine.Symbol.IsVisible = false;

            graphControlTrainingSet.AxisChange();
            graphControlTrainingSet.Invalidate();
        }

        private void LoadLineTestGraph()
        {
            MultipleLinearRegression regresion = new MultipleLinearRegression(
                MatrixOperations.MatrixOps.ArrrayToSingleColumnMatrix(RealTestY),
                MatrixOperations.MatrixOps.ArrrayToSingleColumnMatrix(PredictedY_onTest),
                null, null);

            regresion.Learn();

            double[] rangeNumbers;
            double[] K = regresion.Transform(0, out rangeNumbers);

            GraphPane pane = graphControlTestSet.GraphPane;
            PointPairList linePoints = new PointPairList(rangeNumbers, K);

            LineItem myLine;

            myLine = pane.AddCurve("Line", linePoints, Color.Red, SymbolType.Circle);
            myLine.Line.IsAntiAlias = true;
            myLine.Line.IsVisible = true;
            myLine.Symbol.IsVisible = false;

            graphControlTrainingSet.AxisChange();
            graphControlTrainingSet.Invalidate();
        }

        private void btnSavePrediction_Click(object sender, EventArgs e)
        {
            string selectedPath = "";
            folderBrowserDialog.SelectedPath = AppDomain.CurrentDomain.BaseDirectory;
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                selectedPath = folderBrowserDialog.SelectedPath;

                File.WriteAllText(selectedPath + "\\results.csv" , PrepareDataForCsv());

                MessageBox.Show("Saved!","Success",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private string PrepareDataForCsv()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("ReadTestY,PredictedTestY\n");

            for(int i = 0; i < realTestY.Length; i++)
                builder.Append(realTestY[i].ToString("F2") + "," + predictedY_onTest[i].ToString("F2") + "\n");

            return builder.ToString();
        }
    }
}

