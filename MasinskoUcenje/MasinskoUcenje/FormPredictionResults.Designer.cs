﻿namespace DiplomskiMasinskoUcenje
{
    partial class FormPredictionResults
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.graphControlTrainingSet = new ZedGraph.ZedGraphControl();
            this.label1 = new System.Windows.Forms.Label();
            this.graphControlTestSet = new ZedGraph.ZedGraphControl();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSavePrediction = new System.Windows.Forms.Button();
            this.gridTrainingY = new System.Windows.Forms.DataGridView();
            this.gridTestY = new System.Windows.Forms.DataGridView();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.gridTrainingY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTestY)).BeginInit();
            this.SuspendLayout();
            // 
            // graphControlTrainingSet
            // 
            this.graphControlTrainingSet.Location = new System.Drawing.Point(13, 31);
            this.graphControlTrainingSet.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.graphControlTrainingSet.Name = "graphControlTrainingSet";
            this.graphControlTrainingSet.ScrollGrace = 0D;
            this.graphControlTrainingSet.ScrollMaxX = 0D;
            this.graphControlTrainingSet.ScrollMaxY = 0D;
            this.graphControlTrainingSet.ScrollMaxY2 = 0D;
            this.graphControlTrainingSet.ScrollMinX = 0D;
            this.graphControlTrainingSet.ScrollMinY = 0D;
            this.graphControlTrainingSet.ScrollMinY2 = 0D;
            this.graphControlTrainingSet.Size = new System.Drawing.Size(469, 305);
            this.graphControlTrainingSet.TabIndex = 0;
            this.graphControlTrainingSet.UseExtendedPrintDialog = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(266, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Prediction on training set (learning data):";
            // 
            // graphControlTestSet
            // 
            this.graphControlTestSet.Location = new System.Drawing.Point(13, 371);
            this.graphControlTestSet.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.graphControlTestSet.Name = "graphControlTestSet";
            this.graphControlTestSet.ScrollGrace = 0D;
            this.graphControlTestSet.ScrollMaxX = 0D;
            this.graphControlTestSet.ScrollMaxY = 0D;
            this.graphControlTestSet.ScrollMaxY2 = 0D;
            this.graphControlTestSet.ScrollMinX = 0D;
            this.graphControlTestSet.ScrollMinY = 0D;
            this.graphControlTestSet.ScrollMinY2 = 0D;
            this.graphControlTestSet.Size = new System.Drawing.Size(469, 305);
            this.graphControlTestSet.TabIndex = 2;
            this.graphControlTestSet.UseExtendedPrintDialog = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 344);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(216, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Prediction on test set (new data):";
            // 
            // btnSavePrediction
            // 
            this.btnSavePrediction.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.btnSavePrediction.Location = new System.Drawing.Point(645, 682);
            this.btnSavePrediction.Name = "btnSavePrediction";
            this.btnSavePrediction.Size = new System.Drawing.Size(119, 29);
            this.btnSavePrediction.TabIndex = 4;
            this.btnSavePrediction.Text = "Save Prediction";
            this.btnSavePrediction.UseVisualStyleBackColor = true;
            this.btnSavePrediction.Click += new System.EventHandler(this.btnSavePrediction_Click);
            // 
            // gridTrainingY
            // 
            this.gridTrainingY.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridTrainingY.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.gridTrainingY.Location = new System.Drawing.Point(490, 31);
            this.gridTrainingY.Name = "gridTrainingY";
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.gridTrainingY.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gridTrainingY.RowTemplate.Height = 24;
            this.gridTrainingY.Size = new System.Drawing.Size(274, 305);
            this.gridTrainingY.TabIndex = 5;
            // 
            // gridTestY
            // 
            this.gridTestY.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridTestY.Location = new System.Drawing.Point(490, 371);
            this.gridTestY.Name = "gridTestY";
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.gridTestY.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.gridTestY.RowTemplate.Height = 24;
            this.gridTestY.Size = new System.Drawing.Size(274, 305);
            this.gridTestY.TabIndex = 6;
            // 
            // FormPredictionResults
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.ClientSize = new System.Drawing.Size(776, 720);
            this.Controls.Add(this.gridTestY);
            this.Controls.Add(this.gridTrainingY);
            this.Controls.Add(this.btnSavePrediction);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.graphControlTestSet);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.graphControlTrainingSet);
            this.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Name = "FormPredictionResults";
            this.Text = "FormPredictionResults";
            ((System.ComponentModel.ISupportInitialize)(this.gridTrainingY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridTestY)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ZedGraph.ZedGraphControl graphControlTrainingSet;
        private System.Windows.Forms.Label label1;
        private ZedGraph.ZedGraphControl graphControlTestSet;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSavePrediction;
        private System.Windows.Forms.DataGridView gridTrainingY;
        private System.Windows.Forms.DataGridView gridTestY;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
    }
}