﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiplomskiMasinskoUcenje
{
    public partial class FormShowData : Form
    {
        double[][] dataX;
        double[][] dataY;
        string[] header;

        public FormShowData(double[][] dataX, double[][] dataY, string[] header)
        {
            InitializeComponent();

            this.DataX = dataX;
            this.DataY = dataY;
            this.Header = header;

            LoadData();
        }

        public double[][] DataX { get => dataX; set => dataX = value; }
        public double[][] DataY { get => dataY; set => dataY = value; }
        public string[] Header { get => header; set => header = value; }

        public void RefreshView()
        {
            LoadData();
        }

        private void LoadData()
        {
            gridData.Rows.Clear();
            gridData.Columns.Clear();

            foreach (string headerItem in Header)
                gridData.Columns.Add(headerItem,headerItem);

            foreach (DataGridViewColumn col in gridData.Columns)
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            for(int i = 0; i < DataX.Length; i++)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(gridData);

                for(int j = 0; j < DataX[0].Length + 1; j++)
                {
                    if (j == DataX[0].Length)
                        row.Cells[j].Value = DataY[i][0];
                    else
                        row.Cells[j].Value = DataX[i][j];
                }

                gridData.Rows.Add(row);
            }
        }
    }
}
