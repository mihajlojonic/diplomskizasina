﻿namespace DiplomskiMasinskoUcenje
{
    partial class FormExplorer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelLeftSide = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPredict = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.groupTestOptions = new System.Windows.Forms.GroupBox();
            this.numericPercentage = new System.Windows.Forms.NumericUpDown();
            this.numericCrossValidationFolds = new System.Windows.Forms.NumericUpDown();
            this.radioPercentageSplit = new System.Windows.Forms.RadioButton();
            this.radioCrossValidation = new System.Windows.Forms.RadioButton();
            this.radioUsetrainingTestSet = new System.Windows.Forms.RadioButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trainingSetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testSetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trainingDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.graphControl = new ZedGraph.ZedGraphControl();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnVisuelizeData = new System.Windows.Forms.Button();
            this.comboY = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboX = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.elementHost2 = new System.Windows.Forms.Integration.ElementHost();
            this.gauge1 = new LiveCharts.Wpf.Gauge();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cartCoeff = new System.Windows.Forms.Integration.ElementHost();
            this.cartesianChart1 = new LiveCharts.Wpf.CartesianChart();
            this.panelLeftSide.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupTestOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCrossValidationFolds)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelLeftSide
            // 
            this.panelLeftSide.Controls.Add(this.groupBox1);
            this.panelLeftSide.Controls.Add(this.groupTestOptions);
            this.panelLeftSide.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeftSide.Location = new System.Drawing.Point(0, 28);
            this.panelLeftSide.Name = "panelLeftSide";
            this.panelLeftSide.Size = new System.Drawing.Size(298, 925);
            this.panelLeftSide.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.groupBox1.Controls.Add(this.btnPredict);
            this.groupBox1.Controls.Add(this.btnStart);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox1.Location = new System.Drawing.Point(0, 176);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(298, 749);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Actions";
            // 
            // btnPredict
            // 
            this.btnPredict.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.btnPredict.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.btnPredict.Image = global::DiplomskiMasinskoUcenje.Properties.Resources.icons8_Training_48px;
            this.btnPredict.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPredict.Location = new System.Drawing.Point(67, 96);
            this.btnPredict.Name = "btnPredict";
            this.btnPredict.Size = new System.Drawing.Size(159, 60);
            this.btnPredict.TabIndex = 11;
            this.btnPredict.Text = "Predict";
            this.btnPredict.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPredict.UseVisualStyleBackColor = true;
            this.btnPredict.Click += new System.EventHandler(this.btnPredict_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnStart.Enabled = false;
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.btnStart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.btnStart.Image = global::DiplomskiMasinskoUcenje.Properties.Resources.icons8_Scatter_Plot_48px;
            this.btnStart.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStart.Location = new System.Drawing.Point(67, 21);
            this.btnStart.Name = "btnStart";
            this.btnStart.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnStart.Size = new System.Drawing.Size(159, 60);
            this.btnStart.TabIndex = 7;
            this.btnStart.Text = "Learn";
            this.btnStart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // groupTestOptions
            // 
            this.groupTestOptions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.groupTestOptions.Controls.Add(this.numericPercentage);
            this.groupTestOptions.Controls.Add(this.numericCrossValidationFolds);
            this.groupTestOptions.Controls.Add(this.radioPercentageSplit);
            this.groupTestOptions.Controls.Add(this.radioCrossValidation);
            this.groupTestOptions.Controls.Add(this.radioUsetrainingTestSet);
            this.groupTestOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupTestOptions.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupTestOptions.Location = new System.Drawing.Point(0, 0);
            this.groupTestOptions.Name = "groupTestOptions";
            this.groupTestOptions.Size = new System.Drawing.Size(298, 176);
            this.groupTestOptions.TabIndex = 0;
            this.groupTestOptions.TabStop = false;
            this.groupTestOptions.Text = "Test Options";
            // 
            // numericPercentage
            // 
            this.numericPercentage.Location = new System.Drawing.Point(172, 115);
            this.numericPercentage.Name = "numericPercentage";
            this.numericPercentage.Size = new System.Drawing.Size(120, 22);
            this.numericPercentage.TabIndex = 6;
            this.numericPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericPercentage.Value = new decimal(new int[] {
            66,
            0,
            0,
            0});
            // 
            // numericCrossValidationFolds
            // 
            this.numericCrossValidationFolds.Enabled = false;
            this.numericCrossValidationFolds.Location = new System.Drawing.Point(173, 72);
            this.numericCrossValidationFolds.Name = "numericCrossValidationFolds";
            this.numericCrossValidationFolds.Size = new System.Drawing.Size(119, 22);
            this.numericCrossValidationFolds.TabIndex = 5;
            this.numericCrossValidationFolds.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericCrossValidationFolds.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // radioPercentageSplit
            // 
            this.radioPercentageSplit.AutoSize = true;
            this.radioPercentageSplit.Checked = true;
            this.radioPercentageSplit.Location = new System.Drawing.Point(12, 115);
            this.radioPercentageSplit.Name = "radioPercentageSplit";
            this.radioPercentageSplit.Size = new System.Drawing.Size(131, 21);
            this.radioPercentageSplit.TabIndex = 3;
            this.radioPercentageSplit.TabStop = true;
            this.radioPercentageSplit.Text = "Percentage split";
            this.radioPercentageSplit.UseVisualStyleBackColor = true;
            this.radioPercentageSplit.CheckedChanged += new System.EventHandler(this.radioPercentageSplit_CheckedChanged);
            // 
            // radioCrossValidation
            // 
            this.radioCrossValidation.AutoSize = true;
            this.radioCrossValidation.Location = new System.Drawing.Point(12, 72);
            this.radioCrossValidation.Name = "radioCrossValidation";
            this.radioCrossValidation.Size = new System.Drawing.Size(130, 21);
            this.radioCrossValidation.TabIndex = 2;
            this.radioCrossValidation.Text = "Cross-validation";
            this.radioCrossValidation.UseVisualStyleBackColor = true;
            this.radioCrossValidation.CheckedChanged += new System.EventHandler(this.radioCrossValidation_CheckedChanged);
            // 
            // radioUsetrainingTestSet
            // 
            this.radioUsetrainingTestSet.AutoSize = true;
            this.radioUsetrainingTestSet.Location = new System.Drawing.Point(12, 32);
            this.radioUsetrainingTestSet.Name = "radioUsetrainingTestSet";
            this.radioUsetrainingTestSet.Size = new System.Drawing.Size(202, 21);
            this.radioUsetrainingTestSet.TabIndex = 1;
            this.radioUsetrainingTestSet.Text = "Use Training and Test Sets";
            this.radioUsetrainingTestSet.UseVisualStyleBackColor = true;
            this.radioUsetrainingTestSet.CheckedChanged += new System.EventHandler(this.radioSuppliedTestSet_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1035, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadFileToolStripMenuItem});
            this.fileToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.fileToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadFileToolStripMenuItem
            // 
            this.loadFileToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.loadFileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trainingSetToolStripMenuItem,
            this.testSetToolStripMenuItem});
            this.loadFileToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.loadFileToolStripMenuItem.Name = "loadFileToolStripMenuItem";
            this.loadFileToolStripMenuItem.Size = new System.Drawing.Size(117, 26);
            this.loadFileToolStripMenuItem.Text = "Load";
            this.loadFileToolStripMenuItem.Click += new System.EventHandler(this.loadFileToolStripMenuItem_Click);
            // 
            // trainingSetToolStripMenuItem
            // 
            this.trainingSetToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.trainingSetToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.trainingSetToolStripMenuItem.Name = "trainingSetToolStripMenuItem";
            this.trainingSetToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.trainingSetToolStripMenuItem.Text = "Training Set";
            this.trainingSetToolStripMenuItem.Click += new System.EventHandler(this.trainingSetToolStripMenuItem_Click);
            // 
            // testSetToolStripMenuItem
            // 
            this.testSetToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.testSetToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.testSetToolStripMenuItem.Name = "testSetToolStripMenuItem";
            this.testSetToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.testSetToolStripMenuItem.Text = "Test Set";
            this.testSetToolStripMenuItem.Click += new System.EventHandler(this.testSetToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showDataToolStripMenuItem});
            this.viewToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // showDataToolStripMenuItem
            // 
            this.showDataToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.showDataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trainingDataToolStripMenuItem,
            this.testDataToolStripMenuItem});
            this.showDataToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.showDataToolStripMenuItem.Name = "showDataToolStripMenuItem";
            this.showDataToolStripMenuItem.Size = new System.Drawing.Size(156, 26);
            this.showDataToolStripMenuItem.Text = "Show Data";
            // 
            // trainingDataToolStripMenuItem
            // 
            this.trainingDataToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.trainingDataToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.trainingDataToolStripMenuItem.Name = "trainingDataToolStripMenuItem";
            this.trainingDataToolStripMenuItem.Size = new System.Drawing.Size(171, 26);
            this.trainingDataToolStripMenuItem.Text = "Training data";
            this.trainingDataToolStripMenuItem.Click += new System.EventHandler(this.trainingDataToolStripMenuItem_Click);
            // 
            // testDataToolStripMenuItem
            // 
            this.testDataToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.testDataToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.testDataToolStripMenuItem.Name = "testDataToolStripMenuItem";
            this.testDataToolStripMenuItem.Size = new System.Drawing.Size(171, 26);
            this.testDataToolStripMenuItem.Text = "Test data";
            this.testDataToolStripMenuItem.Click += new System.EventHandler(this.testDataToolStripMenuItem_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.groupBox2.Controls.Add(this.graphControl);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox2.Location = new System.Drawing.Point(298, 28);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(737, 551);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Graph";
            // 
            // graphControl
            // 
            this.graphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphControl.Location = new System.Drawing.Point(3, 60);
            this.graphControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.graphControl.Name = "graphControl";
            this.graphControl.ScrollGrace = 0D;
            this.graphControl.ScrollMaxX = 0D;
            this.graphControl.ScrollMaxY = 0D;
            this.graphControl.ScrollMaxY2 = 0D;
            this.graphControl.ScrollMinX = 0D;
            this.graphControl.ScrollMinY = 0D;
            this.graphControl.ScrollMinY2 = 0D;
            this.graphControl.Size = new System.Drawing.Size(731, 488);
            this.graphControl.TabIndex = 1;
            this.graphControl.UseExtendedPrintDialog = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnVisuelizeData);
            this.groupBox4.Controls.Add(this.comboY);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.comboX);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox4.Location = new System.Drawing.Point(3, 18);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(731, 42);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Axis";
            // 
            // btnVisuelizeData
            // 
            this.btnVisuelizeData.BackColor = System.Drawing.SystemColors.Control;
            this.btnVisuelizeData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.btnVisuelizeData.Location = new System.Drawing.Point(594, 12);
            this.btnVisuelizeData.Name = "btnVisuelizeData";
            this.btnVisuelizeData.Size = new System.Drawing.Size(118, 25);
            this.btnVisuelizeData.TabIndex = 4;
            this.btnVisuelizeData.Text = "Visuelize data";
            this.btnVisuelizeData.UseVisualStyleBackColor = false;
            this.btnVisuelizeData.Click += new System.EventHandler(this.btnVisuelizeData_Click);
            // 
            // comboY
            // 
            this.comboY.FormattingEnabled = true;
            this.comboY.Location = new System.Drawing.Point(339, 13);
            this.comboY.Name = "comboY";
            this.comboY.Size = new System.Drawing.Size(238, 24);
            this.comboY.TabIndex = 3;
            this.comboY.SelectedIndexChanged += new System.EventHandler(this.comboY_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(313, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Y:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "X:";
            // 
            // comboX
            // 
            this.comboX.FormattingEnabled = true;
            this.comboX.Location = new System.Drawing.Point(68, 13);
            this.comboX.Name = "comboX";
            this.comboX.Size = new System.Drawing.Size(238, 24);
            this.comboX.TabIndex = 0;
            this.comboX.SelectedIndexChanged += new System.EventHandler(this.comboX_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.groupBox3.Controls.Add(this.groupBox6);
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox3.Location = new System.Drawing.Point(298, 579);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(737, 374);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Coefficients and Error";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.elementHost2);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox6.Location = new System.Drawing.Point(3, 155);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(731, 216);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "RSquared";
            // 
            // elementHost2
            // 
            this.elementHost2.Location = new System.Drawing.Point(177, 21);
            this.elementHost2.Name = "elementHost2";
            this.elementHost2.Size = new System.Drawing.Size(401, 186);
            this.elementHost2.TabIndex = 2;
            this.elementHost2.Text = "elementHost2";
            this.elementHost2.Child = this.gauge1;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cartCoeff);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox5.Location = new System.Drawing.Point(3, 18);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(731, 137);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Coefficients";
            // 
            // cartCoeff
            // 
            this.cartCoeff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cartCoeff.Location = new System.Drawing.Point(3, 18);
            this.cartCoeff.Name = "cartCoeff";
            this.cartCoeff.Size = new System.Drawing.Size(725, 116);
            this.cartCoeff.TabIndex = 0;
            this.cartCoeff.Text = "elementHost1";
            this.cartCoeff.Child = this.cartesianChart1;
            // 
            // FormExplorer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1035, 953);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panelLeftSide);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormExplorer";
            this.Text = "Linear Regression Explorer";
            this.panelLeftSide.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupTestOptions.ResumeLayout(false);
            this.groupTestOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCrossValidationFolds)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelLeftSide;
        private System.Windows.Forms.GroupBox groupTestOptions;
        private System.Windows.Forms.NumericUpDown numericPercentage;
        private System.Windows.Forms.NumericUpDown numericCrossValidationFolds;
        private System.Windows.Forms.RadioButton radioPercentageSplit;
        private System.Windows.Forms.RadioButton radioCrossValidation;
        private System.Windows.Forms.RadioButton radioUsetrainingTestSet;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadFileToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox2;
        private ZedGraph.ZedGraphControl graphControl;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comboY;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboX;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnVisuelizeData;
        private System.Windows.Forms.ToolStripMenuItem trainingSetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testSetToolStripMenuItem;
        private System.Windows.Forms.Button btnPredict;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trainingDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testDataToolStripMenuItem;
        private System.Windows.Forms.Integration.ElementHost cartCoeff;
        private LiveCharts.Wpf.CartesianChart cartesianChart1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Integration.ElementHost elementHost2;
        private LiveCharts.Wpf.Gauge gauge1;
    }
}