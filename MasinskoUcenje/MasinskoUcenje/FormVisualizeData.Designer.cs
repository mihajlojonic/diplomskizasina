﻿namespace DiplomskiMasinskoUcenje
{
    partial class FormVisualizeData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.graphHistogram = new ZedGraph.ZedGraphControl();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.cartesianChart1 = new LiveCharts.Wpf.CartesianChart();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.comboColumn = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMin = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblMax = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblAvg = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblStdev = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // graphHistogram
            // 
            this.graphHistogram.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphHistogram.Location = new System.Drawing.Point(3, 18);
            this.graphHistogram.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.graphHistogram.Name = "graphHistogram";
            this.graphHistogram.ScrollGrace = 0D;
            this.graphHistogram.ScrollMaxX = 0D;
            this.graphHistogram.ScrollMaxY = 0D;
            this.graphHistogram.ScrollMaxY2 = 0D;
            this.graphHistogram.ScrollMinX = 0D;
            this.graphHistogram.ScrollMinY = 0D;
            this.graphHistogram.ScrollMinY2 = 0D;
            this.graphHistogram.Size = new System.Drawing.Size(1120, 310);
            this.graphHistogram.TabIndex = 0;
            this.graphHistogram.UseExtendedPrintDialog = true;
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(3, 18);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(1126, 428);
            this.elementHost1.TabIndex = 1;
            this.elementHost1.Text = "cartesianChart1";
            this.elementHost1.Child = this.cartesianChart1;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.groupBox1.Controls.Add(this.elementHost1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1132, 449);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Correlation";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(45)))), ((int)(((byte)(49)))));
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox2.Location = new System.Drawing.Point(0, 449);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1132, 606);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Column";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lblStdev);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.lblAvg);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.lblMax);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.lblMin);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox5.Location = new System.Drawing.Point(3, 392);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1126, 211);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Min, Max, Avg, Std";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.graphHistogram);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox4.Location = new System.Drawing.Point(3, 61);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1126, 331);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Histogram";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.comboColumn);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox3.Location = new System.Drawing.Point(3, 18);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1126, 43);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Choose column";
            // 
            // comboColumn
            // 
            this.comboColumn.FormattingEnabled = true;
            this.comboColumn.Location = new System.Drawing.Point(396, 12);
            this.comboColumn.Name = "comboColumn";
            this.comboColumn.Size = new System.Drawing.Size(295, 24);
            this.comboColumn.TabIndex = 1;
            this.comboColumn.SelectedIndexChanged += new System.EventHandler(this.comboColumn_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.label1.Location = new System.Drawing.Point(27, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Min:";
            // 
            // lblMin
            // 
            this.lblMin.AutoSize = true;
            this.lblMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.lblMin.Location = new System.Drawing.Point(128, 52);
            this.lblMin.Name = "lblMin";
            this.lblMin.Size = new System.Drawing.Size(122, 39);
            this.lblMin.TabIndex = 1;
            this.lblMin.Text = "100.00";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.label3.Location = new System.Drawing.Point(284, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 39);
            this.label3.TabIndex = 2;
            this.label3.Text = "Max:";
            // 
            // lblMax
            // 
            this.lblMax.AutoSize = true;
            this.lblMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.lblMax.Location = new System.Drawing.Point(401, 52);
            this.lblMax.Name = "lblMax";
            this.lblMax.Size = new System.Drawing.Size(122, 39);
            this.lblMax.TabIndex = 3;
            this.lblMax.Text = "100.00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.label5.Location = new System.Drawing.Point(556, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 39);
            this.label5.TabIndex = 4;
            this.label5.Text = "Avg:";
            // 
            // lblAvg
            // 
            this.lblAvg.AutoSize = true;
            this.lblAvg.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.lblAvg.Location = new System.Drawing.Point(661, 52);
            this.lblAvg.Name = "lblAvg";
            this.lblAvg.Size = new System.Drawing.Size(122, 39);
            this.lblAvg.TabIndex = 5;
            this.lblAvg.Text = "100.00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.label7.Location = new System.Drawing.Point(813, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 39);
            this.label7.TabIndex = 6;
            this.label7.Text = "Stdev:";
            // 
            // lblStdev
            // 
            this.lblStdev.AutoSize = true;
            this.lblStdev.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.lblStdev.Location = new System.Drawing.Point(951, 52);
            this.lblStdev.Name = "lblStdev";
            this.lblStdev.Size = new System.Drawing.Size(122, 39);
            this.lblStdev.TabIndex = 7;
            this.lblStdev.Text = "100.00";
            // 
            // FormVisualizeData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1132, 1055);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormVisualizeData";
            this.Text = "FormVisualizeData";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ZedGraph.ZedGraphControl graphHistogram;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private LiveCharts.Wpf.CartesianChart cartesianChart1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox comboColumn;
        private System.Windows.Forms.Label lblStdev;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblAvg;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblMax;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblMin;
        private System.Windows.Forms.Label label1;
    }
}