﻿using LinearRegression;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiplomskiMasinskoUcenje
{
    public partial class FormLoadFile : Form
    {
        public FileSettings Data { get; set; }

        public EventHandler OkButtonClicked;

        public FormLoadFile(FileSettings fsc, string title)
        {
            InitializeComponent();
            this.Text = title;
            this.Data = fsc;
            string fileLinkDefault = AppDomain.CurrentDomain.BaseDirectory + "\\bostondata_scaled.csv";
            this.Data.FileLink = fileLinkDefault;
            txtFileLink.Text = this.Data.FileLink;
        }

        public virtual void OnOkButtonClicked(EventArgs e)
        {
            OkButtonClicked?.Invoke(this, e);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Data.Header = checkBoxHeader.Checked;

            Data.Sep = txtRegex.Text;

            OnOkButtonClicked(e);

            this.Close();
        }



        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnChoseLink_Click(object sender, EventArgs e)
        {
            // Show the dialog and get result.
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                txtFileLink.Text = openFileDialog.FileName;
                Data.FileLink = openFileDialog.FileName;
            }
        }

        

        private void FormLoadFile_Load(object sender, EventArgs e)
        {
            txtRegex.Enabled = true; 
        }
    }
}
