﻿using LinearRegression;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using ZedGraph;


namespace DiplomskiMasinskoUcenje
{
    public partial class FormVisualizeData : Form
    {
        DataDescription descData;
        string[] header;

        double[][] data;


        public FormVisualizeData(DataDescription descData,  double[][] dataX, double[][] dataY, string[] header)
        {
            InitializeComponent();

            this.descData = descData;
            this.header = header;

            data = MatrixOperations.MatrixOps.ConcatenateMatrixFromRight(dataX, dataY);

            comboColumn.Items.AddRange(header);
            comboColumn.SelectedIndex = 0;

            LoadDataInHistogram();
            HeatChart();
            LoadMinMaxAvgStdev();
           
        }

        private void HeatChart()
        {
            HeatSeries heatSeries = new HeatSeries() { DataLabels = true };
            heatSeries.GradientStopCollection = new GradientStopCollection
             {
                    new GradientStop(System.Windows.Media.Color.FromRgb(0, 0, 0), -1),
                    new GradientStop(System.Windows.Media.Color.FromRgb(0, 0, 1), -0.99),
                    new GradientStop(System.Windows.Media.Color.FromRgb(255, 0, 0), 0),
                    new GradientStop(System.Windows.Media.Color.FromRgb(0, 0, 2), 0.99),
                    new GradientStop(System.Windows.Media.Color.FromRgb(0, 0, 3), 1)
            };

            heatSeries.DrawsHeatRange = false;

            ChartValues<HeatPoint> heatPoints = new ChartValues<HeatPoint>();
            
            for (int i = 0; i < descData.corr.GetLength(0); i++)
                for(int j = 0; j < descData.corr.GetLength(1); j++)
                    heatPoints.Add(new HeatPoint(i, descData.corr.GetLength(1) - j - 1, Math.Truncate(descData.corr[i, j] * 100) / 100));


            heatSeries.Values = heatPoints;

            cartesianChart1.AxisX.Add(new LiveCharts.Wpf.Axis
            {
                Foreground = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255,255,255)),
                LabelsRotation = -90,
                Labels = CutHeaderLabels(header.Reverse().ToArray()),
                Separator = new Separator { Step = 1 }
            });

            

            cartesianChart1.AxisY.Add(new LiveCharts.Wpf.Axis
            {
                Foreground = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 255, 255)),
                LabelsRotation = 0,
                Labels = header,
                Separator = new Separator { Step = 1 }
            });


            cartesianChart1.Series.Add(heatSeries);
            
            cartesianChart1.InvalidateArrange();
            cartesianChart1.InvalidateMeasure();
            cartesianChart1.InvalidateVisual();
        }
        

        private void LoadDataInHistogram()
        {
            GraphPane myPane = graphHistogram.GraphPane;
            myPane.CurveList.Clear();

            myPane.Title.IsVisible = false;
            myPane.Legend.IsVisible = false;
            myPane.Border.IsVisible = false;
            myPane.Border.IsVisible = false;
            myPane.Margin.Bottom = 20f;
            myPane.Margin.Right = 20f;
            myPane.Margin.Left = 20f;
            myPane.Margin.Top = 30f;

            myPane.YAxis.Title.IsVisible = true;
            myPane.YAxis.IsVisible = true;
            myPane.YAxis.MinorGrid.IsVisible = false;
            myPane.YAxis.MajorGrid.IsVisible = false;
            myPane.YAxis.IsAxisSegmentVisible = false;
            myPane.YAxis.Scale.Max = data.Length / 2;
            myPane.YAxis.Scale.Min = 0;
            myPane.YAxis.MajorGrid.IsZeroLine = false;
            myPane.YAxis.Title.Text = "Classes";
            myPane.YAxis.MinorTic.IsOpposite = false;
            myPane.YAxis.MajorTic.IsOpposite = false;
            myPane.YAxis.MinorTic.IsInside = false;
            myPane.YAxis.MajorTic.IsInside = false;
            myPane.YAxis.MinorTic.IsOutside = false;
            myPane.YAxis.MajorTic.IsOutside = false;

            myPane.XAxis.MinorTic.IsOpposite = false;
            myPane.XAxis.MajorTic.IsOpposite = false;
            myPane.XAxis.Title.IsVisible = true;
            myPane.XAxis.Title.Text = "Relative class response";
            myPane.XAxis.IsVisible = true;
            myPane.XAxis.Scale.Min = descData.min[comboColumn.SelectedIndex];
            myPane.XAxis.Scale.Max = descData.max[comboColumn.SelectedIndex];
            myPane.XAxis.IsAxisSegmentVisible = false;
            myPane.XAxis.MajorGrid.IsVisible = false;
            myPane.XAxis.MajorGrid.IsZeroLine = false;
            myPane.XAxis.MinorTic.IsOpposite = false;
            myPane.XAxis.MinorTic.IsInside = false;
            myPane.XAxis.MinorTic.IsOutside = false;

            PointPairList list = new PointPairList();

            double[] edges = new double[10];
            int[] discriminants = GetDistributionForColumn(comboColumn.SelectedIndex, 10, out edges);

            myPane.XAxis.Scale.Min = edges[0];
            myPane.XAxis.Scale.Max = edges[edges.Length - 1];

            for (int i = 0; i < discriminants.Length; i++)
                list.Add(edges[i], discriminants[i] * 1);
            BarItem myCurve = myPane.AddBar("b", list, System.Drawing.Color.DarkBlue);
            
            myPane.BarSettings.Base = BarBase.X;
            graphHistogram.AxisChange();
            graphHistogram.Invalidate();
        }

        private void LoadMinMaxAvgStdev()
        {
            int col = comboColumn.SelectedIndex;

            lblMin.Text = descData.min[col].ToString("F2");
            lblMax.Text = descData.max[col].ToString("F2");
            lblAvg.Text = descData.mean[col].ToString("F2");
            lblStdev.Text = descData.stdev[col].ToString("F2");
        }

        public void RefreshView()
        {
            LoadDataInHistogram();
            HeatChart();
            LoadMinMaxAvgStdev();
        }

        private string[] CutHeaderLabels(string [] header)
        {
            string[] retHeader = new string[header.Length];

            for (int i = 0; i < header.Length; i++)
                retHeader[i] = new string(header[i].ToCharArray().Take(3).ToArray());

            return retHeader;
        }

        private int[] GetDistributionForColumn(int columnIndex, int numBars, out double[] edges)
        {
            int[] retArr = new int[numBars];

            double min = descData.min[columnIndex];
            double max = descData.max[columnIndex];

            double chuck = (max - min) / (numBars - 1);

            edges = new double[numBars];
            edges[0] = min;
            edges[numBars - 1] = max;

            for (int i = 1; i < numBars - 1; i++)
                edges[i] = min + (chuck * i);

            bool seted = false;
            for(int i = 0; i < data.Length; i++)
            {
                seted = false;
                for(int k = 0; k <edges.Length - 1; k++)
                {
                    if (data[i][columnIndex] >= edges[k] && data[i][columnIndex] < edges[k + 1])
                    {
                        retArr[k]++;
                        seted = true;
                        break;
                    }
                }

                if(!seted)
                {
                    retArr[numBars - 1]++;
                    seted = false;
                }
                    
            }

            return retArr;
        }

        private void comboColumn_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataInHistogram();
            LoadMinMaxAvgStdev();
        }
    }
}
