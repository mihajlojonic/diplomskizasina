﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace MatrixOperations
{
    public static class MatrixOps
    {

        public static double[][] ConcatenateMatrixFromRight(double[][] firstMat, double[][] secondMat)
        {
            int rows = firstMat.Length;
            int cols = firstMat[0].Length;

            double[][] dataRet = MatrixCreate(rows, cols + 1);

            for(int j = 0; j < dataRet[0].Length; j++)
            {
                for(int i = 0; i < dataRet.Length; i++)
                {
                    if (j == dataRet[0].Length - 1)
                        dataRet[i][j] = secondMat[i][0];
                    else
                        dataRet[i][j] = firstMat[i][j];
                }
            }

            return dataRet;
        }

        public static double[][] ArrrayToSingleColumnMatrix(double[] array)
        {
            double[][] matrix = MatrixCreate(array.Length, 1);

            for (int i = 0; i < array.Length; i++)
                matrix[i][0] = array[i];

            return matrix;
        }

        public static T[][] SetColumnOnValue<T>(T[][] jaggedArray, int wanted_column, T value)
        {
            for(int i = 0; i < jaggedArray.Length;i++)
            {
                jaggedArray[i][wanted_column] = value;
            }

            return jaggedArray;
        }

        public static T[][] ReplaceColumn<T>(T[][] jaggedArray, T[] col, int columnForReplace)
        {
            for (int i = 0; i < jaggedArray.Length; i++)
                jaggedArray[i][columnForReplace] = col[i];

            return jaggedArray;
        }

        public static T[] column<T>(T[][] jaggedArray, int wanted_column)
        {
            T[] columnArray = new T[jaggedArray.Length];
            T[] rowArray;
            for (int i = 0; i < jaggedArray.Length; i++)
            {
                rowArray = jaggedArray[i];
                if (wanted_column < rowArray.Length)
                    columnArray[i] = rowArray[wanted_column];
            }
            return columnArray;
        }

        public static double[][] MatrixLoad(string file, bool header, string sepPattern, out string[] headerArr)
        {
            headerArr = null;
            string line = "";
            string[] tokens = null;
            int ct = 0;
            int rows, cols;

            System.IO.FileStream ifs = new System.IO.FileStream(file, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            System.IO.StreamReader sr = new System.IO.StreamReader(ifs);

            while ((line = sr.ReadLine()) != null)
            {
                ++ct;
                tokens = Regex.Split(line, sepPattern);
            }
            sr.Close(); ifs.Close();

            if (header == true)
                rows = ct - 1;
            else
                rows = ct;

            cols = tokens.Length;
            double[][] result = MatrixCreate(rows, cols);

            int i = 0;
            ifs = new System.IO.FileStream(file, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            sr = new System.IO.StreamReader(ifs);

            if (header == true)
            {
                line = sr.ReadLine();
                headerArr = Regex.Split(line, sepPattern);  
            }

            int numCol = 0;
            while ((line = sr.ReadLine()) != null)
            {
                tokens = Regex.Split(line, sepPattern);
                numCol = tokens.Length;
                for (int j = 0; j < cols; ++j)
                    result[i][j] = double.Parse(tokens[j]);
                ++i; 
            }

            if(header == false)
            {
                headerArr = new string[numCol];
                for(int m = 0; m < headerArr.Length; m++)
                {
                    headerArr[m] = "col" + m.ToString();
                }
            }

            sr.Close(); ifs.Close();
            return result;
        }

        public static double[][] MatrixInverse(double[][] matrix)
        {
            int n = matrix.Length;
            double[][] result = MatrixCreate(n, n); 
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j)
                    result[i][j] = matrix[i][j];

            double[][] lum; 
            int[] perm;
            int toggle;
            toggle = MatrixDecompose(matrix, out lum, out perm);

            double[] b = new double[n];
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                    if (i == perm[j])
                        b[j] = 1.0;
                    else
                        b[j] = 0.0;

                double[] x = Helper(lum, b); 
                for (int j = 0; j < n; ++j)
                    result[j][i] = x[j];
            }
            return result;
        } 

        public static int MatrixDecompose(double[][] m, out double[][] lum, out int[] perm)
        {
            int toggle = +1; 
            int n = m.Length;

            lum = MatrixCreate(n, n);
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j)
                    lum[i][j] = m[i][j];

            perm = new int[n];
            for (int i = 0; i < n; ++i)
                perm[i] = i;

            for (int j = 0; j < n - 1; ++j) 
            {
                double max = Math.Abs(lum[j][j]);
                int piv = j;

                for (int i = j + 1; i < n; ++i)
                {
                    double xij = Math.Abs(lum[i][j]);
                    if (xij > max)
                    {
                        max = xij;
                        piv = i;
                    }
                } 

                if (piv != j)
                {
                    double[] tmp = lum[piv]; 
                    lum[piv] = lum[j];
                    lum[j] = tmp;

                    int t = perm[piv]; 
                    perm[piv] = perm[j];
                    perm[j] = t;

                    toggle = -toggle;
                }

                double xjj = lum[j][j];
                if (xjj != 0.0)
                {
                    for (int i = j + 1; i < n; ++i)
                    {
                        double xij = lum[i][j] / xjj;
                        lum[i][j] = xij;
                        for (int k = j + 1; k < n; ++k)
                            lum[i][k] -= xij * lum[j][k];
                    }
                }

            } 

            return toggle;
        } 

        public static double[] MatrixToVector(double[][] matrix)
        {
            int rows = matrix.Length;
            int cols = matrix[0].Length;
            if (cols != 1)
                throw new Exception("Bad matrix");
            double[] result = new double[rows];
            for (int i = 0; i < rows; ++i)
                result[i] = matrix[i][0];
            return result;
        }

        public static double[][] MatrixTranspose(double[][] matrix)
        {
            int rows = matrix.Length;
            int cols = matrix[0].Length;
            double[][] result = MatrixCreate(cols, rows); 
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < cols; ++j)
                {
                    result[j][i] = matrix[i][j];
                }
            }

            return result;
        } 

        public static double[] Helper(double[][] luMatrix, double[] b) 
        {
            int n = luMatrix.Length;
            double[] x = new double[n];
            b.CopyTo(x, 0);

            for (int i = 1; i < n; ++i)
            {
                double sum = x[i];
                for (int j = 0; j < i; ++j)
                    sum -= luMatrix[i][j] * x[j];
                x[i] = sum;
            }

            x[n - 1] /= luMatrix[n - 1][n - 1];
            for (int i = n - 2; i >= 0; --i)
            {
                double sum = x[i];
                for (int j = i + 1; j < n; ++j)
                    sum -= luMatrix[i][j] * x[j];
                x[i] = sum / luMatrix[i][i];
            }

            return x;
        } 

        public static double MatrixDeterminant(double[][] matrix)
        {
            double[][] lum;
            int[] perm;
            int toggle = MatrixDecompose(matrix, out lum, out perm);
            double result = toggle;
            for (int i = 0; i < lum.Length; ++i)
                result *= lum[i][i];
            return result;
        }

        // ----------------------------------------------------------------

        public static double[][] MatrixCreate(int rows, int cols)
        {
            double[][] result = new double[rows][];
            for (int i = 0; i < rows; ++i)
                result[i] = new double[cols];
            return result;
        }

        public static double[][] MatrixProduct(double[][] matrixA,
          double[][] matrixB)
        {
            int aRows = matrixA.Length;
            int aCols = matrixA[0].Length;
            int bRows = matrixB.Length;
            int bCols = matrixB[0].Length;
            if (aCols != bRows)
                throw new Exception("Non-conformable matrices");

            double[][] result = MatrixCreate(aRows, bCols);

            for (int i = 0; i < aRows; ++i) 
                for (int j = 0; j < bCols; ++j) 
                    for (int k = 0; k < aCols; ++k) 
                        result[i][j] += matrixA[i][k] * matrixB[k][j];

            return result;
        }

        public static string MatrixAsString(double[][] matrix)
        {
            string s = "";
            for (int i = 0; i < matrix.Length; ++i)
            {
                for (int j = 0; j < matrix[i].Length; ++j)
                    s += matrix[i][j].ToString("F3").PadLeft(8) + " ";
                s += Environment.NewLine;
            }
            return s;
        }

        public static double[][] ExtractLower(double[][] lum)
        {
            int n = lum.Length;
            double[][] result = MatrixCreate(n, n);
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    if (i == j)
                        result[i][j] = 1.0;
                    else if (i > j)
                        result[i][j] = lum[i][j];
                }
            }
            return result;
        }

        public static double[][] ExtractUpper(double[][] lum)
        {
            int n = lum.Length;
            double[][] result = MatrixCreate(n, n);
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    if (i <= j)
                        result[i][j] = lum[i][j];
                }
            }
            return result;
        }
    }
}
