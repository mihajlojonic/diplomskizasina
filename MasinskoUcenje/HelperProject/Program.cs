﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HelperProject
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string file = @"E:\sklearnproj\data\bostondata.csv";
            /*string sepPattern = "[ ]{1,}";
            bool header = true;
            string[] headerValue = null;
            string line = "";
            string[] tokens = null;
            int ct = 0;
            int rows, cols;
            // determined # rows and cols
            System.IO.FileStream ifs = new System.IO.FileStream(file, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            System.IO.StreamReader sr = new System.IO.StreamReader(ifs);

            while ((line = sr.ReadLine()) != null)
            {
                ++ct;
                //tokens = line.Split(sep); // do validation here
                tokens = Regex.Split(line, "[ ]{1,}");
            }
            sr.Close(); ifs.Close();

            if (header == true)
                rows = ct - 1;
            else
                rows = ct;

            cols = tokens.Length;
            double[][] result = MatrixCreate(rows, cols);

            // load
            int i = 0; // row index
            ifs = new System.IO.FileStream(file, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            sr = new System.IO.StreamReader(ifs);

            if (header == true)
            {
                line = sr.ReadLine();
                headerValue = Regex.Split(line, sepPattern);  // consume header
            }


            while ((line = sr.ReadLine()) != null)
            {
                //tokens = line.Split(sep);
                tokens = Regex.Split(line, "[ ]{1,}");
                for (int j = 0; j < cols; ++j)
                    result[i][j] = double.Parse(tokens[j]);
                ++i; // next row
            }

            sr.Close(); ifs.Close();*/
            /*
            PercentageSplitter ps = new PercentageSplitter(file, 66);

            LinearRegression.LinearRegression linreg = new LinearRegression.LinearRegression(ps.TrainData, ps.TestData, ps.TrainData[0].Length - 1);
            linreg.Learn();

            Console.WriteLine("Coefs:");
            foreach(double d in linreg.Coefficient)
            {
                Console.Write(d.ToString() + ", ");
            }
            Console.WriteLine();

            Console.WriteLine("Computing R-squared\n");
            double R2 = linreg.RSquared(); // use initial data
            Console.WriteLine("R-squared = " + R2.ToString("F4"));

            Console.WriteLine("\nPredicting");
            linreg.PredictY();

            for(int i = 0; i < linreg.PredictedY.Length; i++)
            {
                Console.WriteLine(i + ": predicted: " + linreg.PredictedY[i] + ", real: " + linreg.TestData[i][linreg.TestData[0].Length - 1]);
            }*/
        }

        public static double[][] MatrixCreate(int rows, int cols)
        {
            // allocates/creates a matrix initialized to all 0.0
            // do error checking here
            double[][] result = new double[rows][];
            for (int i = 0; i < rows; ++i)
                result[i] = new double[cols];
            return result;
        }
    }
}
